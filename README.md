
![howl_themes](howl_themes.png)

Theme bundle for the Howl editor (http://howl.io/)

## Installation

Clone or download into `~/.howl/bundles`.

You may also just copy paste this:

`git clone https://gitlab.com/MarcusE1W/marcus-howl-themes ~/.howl/bundles/`

## Contains
- **Eastend** (V0.1 Oct 2018) A light caffee late theme that should work well in changing light conditions
![Screenshot](eastend.png)
