theme = howl.ui.theme

themes = {
  'Eastend': bundle_file('eastend.moon'),
  -- 'XXX': bundle_file('YYY'),
}

for name,file in pairs themes
  theme.register(name, file)

unload = ->
  for name in pairs themes
    theme.unregister(name)

{
  info: {
    author: 'MarcusE1W',
    description: 'A caffee late theme',
    license: 'MIT',
  },
  :unload
}
