{:delegate_to} = howl.util.table

------------------
-- Eastend colours
------------------

-- aqua
-- emerald            = '#2ecc71'
-- greenSea           = '#16a085'
peterRiver         = '#61aeee'  -- blueish
ocean              = '#3385ff' --darker blue

seaForest          = '#00cc00' -- quite bright green
-- coffeeTree         = '#00b33c' -- some green
pear               = '#B6DB2F' --greenish
pear_darker           = '#7d981b'

pumpkin            = '#d35400'
carrot             = '#e67e22'  --orange
orange             = '#f39c12'

saffron            = '#F7B32B'
-- mellow_yellow      = '#FFDE7C'
-- vanilla            = '#F0DDA6'
moccasin           = '#f9f2e0'
moccasin_light     = '#fbf6ea' -- even lighter
moccasin_dark      = '#f7edd4' -- a bit darker

coffee             = '#bf8040'  -- nice brown
tidebrown          = '#444040'
tidebrown_darker   = '#282525'

smog               = '#a4a08e'  -- grey

pink =          '#ed73ec'
-- magenta

ui_background       = tidebrown
ui_txt              = '#d9d7ca'
ui_txt_selected     = '#FFD08C'
syntax_background   = moccasin_light
syntax_txt          = tidebrown_darker -- darker than ui_background



background   = '#0E1F23'  --do not use anymore
selection    = '#759557'  --do not use anymore
foreground   = '#ffffff'  --do not use anymore
red          = '#ff9da4'
yellow_dark  = '#75715e'
blue         = '#86AAD7'
purple       = '#A282DB'
grey_light   = '#a6a6a6'
grey         = '#595959'
grey_darker  = '#383830'
grey_darkest = '#243336'
embedded_bg  = '#484848'
border_color = '#333333'
eee          = '#EEEEEE'

------------------------------------------------------------
-- General styling for context boxes (editor window, command_line)
------------------------------------------------------------
content_box = {
  -- editor window background (textcolor is defined later)
  background:
    color: syntax_background

  border:
    width: 1
    color: border_color

  border_right:
    width: 3
    color: border_color

  border_bottom:
    width: 3
    color: border_color

  header:
    -- top bar over a view
    background:
      color: ui_background

    border_bottom:
      color: grey_darker

    color: orange
    font: bold: false
    padding: 1

  footer:
    -- bar under a view ? or the whole window ?
    background:
      color: ui_background

    border_top:
      color: grey_darker

    color: grey
    font: bold: true
    padding: 1
}

----------------------------------------------
-- editor window
----------------------------------------------

return {
  window:
    -- outer frame around the window
    background:
      color: ui_background

    status:
      font: bold: true, italic: true
      color: grey

      -- info text in command line, e.g. file saved
      -- info: color: grey_light
      info: color: pear

      warning: color: orange

      'error': color: red

  :content_box

  -- Autocomplete box
  popup:
    background:
      color: moccasin
    border:
      color: grey

  editor: delegate_to content_box, {

    -- view title text, header and footer text
    indicators:
      default:
        color: ui_txt_selected

      title:
        font: bold: false

      vi:
        font: bold: true

    caret:
      color: grey_light
      width: 1


    -------------
    -- Line
    -------------
    -- text colour of current line
    current_line:
      background: ui_txt

    ------------------------
    -- Gutter / Line numbers
    ------------------------
    -- the gutter for the line numbers
    gutter:
      color: ui_txt
      background:
        color: ui_background
        alpha: 0.9
  }


  flairs:

    -- the indentation lines
    indentation_guide:
      type: flair.PIPE,
      foreground: smog,
      :background,
      line_width: 1

    indentation_guide_1:
      type: flair.PIPE,
      foreground: grey_darker,
      line_width: 1

    indentation_guide_2:
      type: flair.PIPE,
      foreground: grey_darker,
      line_width: 1

    indentation_guide_3:
      type: flair.PIPE,
      foreground: grey_darker,
      line_width: 1

    edge_line:
      type: flair.PIPE,
      foreground: blue,
      foreground_alpha: 0.3,
      line_width: 0.5


    --------------------------
    -- Search/replace results
    --------------------------
      -- mark search results, also linting results ??
      -- selected result
    search:
      type: highlight.ROUNDED_RECTANGLE
      foreground: black
      foreground_alpha: 1
      background: pink
      text_color: grey_darkest
      height: 'text'

      -- other results
    search_secondary:
      type: flair.ROUNDED_RECTANGLE
      background: yellow_dark
      text_color: grey_darkest
      height: 'text'

    replace_strikeout:
      type: flair.ROUNDED_RECTANGLE
      foreground: black
      background: red
      text_color: black
      height: 'text'


    ----------------------
    -- Braces
    ----------------------
    -- selected curly braces etc. {}
    brace_highlight:
      type: flair.RECTANGLE
      text_color: foreground
      background: pink
      height: 'text'

    -- when you are directly next to braces ??
    brace_highlight_secondary:
      type: flair.RECTANGLE
      foreground: pink
      text_color: syntax_txt
      line_width: 1
      height: 'text'



    list_selection:
      type: flair.RECTANGLE
      -- background: current
      background: pear
      background_alpha: 0.3

    list_highlight:
      type: highlight.UNDERLINE
      -- foreground: eee
      foreground: orange

      -- marks the match in list searches, command palette etc.
      -- text_color: eee
      text_color: peterRiver
      line_width: 2

    ---------------
    -- Cursor
    ---------------
    --cursor colour and width
    cursor:
      type: flair.RECTANGLE
      background: saffron
      width: 3
      height: 'text'

      -- not sure how that is used, change cursor to block cursor ?
    block_cursor:
      type: flair.ROUNDED_RECTANGLE,
      -- background: foreground
      background: saffron
      -- text_color: background
      text_color: syntax_background
      height: 'text',
      min_width: 'letter'

      -- selected text in buffer
    selection:
      type: highlight.ROUNDED_RECTANGLE
      background: selection
      background_alpha: 0.4
      min_width: 'letter'

--------------------------------------------------
-- programming language styles
--------------------------------------------------
-- these styles have to/can be used in the lexer file of the respective language


  styles:
    default:
      -- default text colour
      -- also list selection
      color: syntax_txt

    -- what's this ?
    popup:
      background: grey_darkest
      color: foreground

    comment:
      font: italic: true
      color: smog

    -- Colour for variables/constants
    variable:
      color: carrot

    label:
      color: orange
      font: italic: true

    key:
      color: purple
      font: bold: false

    fdecl: -- function declaration
      color: pear_darker
      font: bold: false

    keyword:
      color: blue
      font: bold: false

    class:
      color: pink
      font: bold: true

    -- or module or so
    type_def:
      color: pear_darker
      font:
        bold: true

    definition:
      color: carrot

    function:
      color: ocean
      font: bold: true

    type:
      color: pear_darker
      font: italic: true

    char: color: coffee
    number: color: orange
    operator: color: blue
    preproc: color: ocean
    special: color: purple
    tag: color: purple
    member: color: red
    info: color: blue


    constant:
      color: carrot

    -- string values and
    -- command name in command palette
    string:
      color: coffee

    regex:
      color: seaForest
      background: embedded_bg

    -- e.g. markdown code
    embedded:
      color: ocean
      background: moccasin_dark



    -- error message e.g. in command line
    error:
      font: italic: true
      color: eee
      background: pumpkin

    warning:
      font: italic: true
      color: orange

    ----------------------------------------------------
    -- Markdown and visual styles
    ----------------------------------------------------

      -- markdown header level 1
    h1:
      font: bold: true
            -- italic: true
      color: pear_darker

    -- markdown header level 1
    h2:
      font: italic: true
      color: pear_darker

    -- markdown header level 3
    h3:
      -- font: italic: true
      color: pear_darker

    emphasis:
      font:
        bold: true
        italic: true

    strong: font: italic: true
    link_label: color: ocean
    link_url: color: smog

    -- not used in markdown
    table:
      color: blue
      background: embedded_bg
      underline: true


    -- git ??
    addition: color: pear
    deletion: color: red
    change: color: peterRiver
  }
